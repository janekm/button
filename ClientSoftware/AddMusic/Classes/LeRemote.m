//
//  LeRemote.m
//  AddMusic
//
//  Created by Janek Mann on 11/11/2012.
//
//

#import "LeRemote.h"

@implementation LeRemote
@synthesize currentlyDisplayingService;
@synthesize connectedServices;
@synthesize remoteDelegate;

#pragma mark -
#pragma mark Init
/****************************************************************************/
/*									Init									*/
/****************************************************************************/
+ (id) sharedInstance
{
	static LeRemote	*this	= nil;
    
	if (!this)
		this = [[LeRemote alloc] init];
    
	return this;
}


- (id) init
{
    self = [super init];
    if (self) {
	}
    return self;
}


- (void) dealloc
{
    // We are a singleton and as such, dealloc shouldn't be called.
    assert(NO);
    [super dealloc];
}

- (void) setup
{
    connectedServices = [NSMutableArray new];

    [[LeDiscovery sharedInstance] setDiscoveryDelegate:self];
    [[LeDiscovery sharedInstance] setPeripheralDelegate:self];
    [[LeDiscovery sharedInstance] startScanningForUUIDString:kTemperatureServiceUUIDString];
}

#pragma mark -
#pragma mark LeTemperatureAlarm Interactions
/****************************************************************************/
/*                  LeTemperatureAlarm Interactions                         */
/****************************************************************************/
- (LeTemperatureAlarmService*) serviceForPeripheral:(CBPeripheral *)peripheral
{
    for (LeTemperatureAlarmService *service in connectedServices) {
        if ( [[service peripheral] isEqual:peripheral] ) {
            return service;
        }
    }
    
    return nil;
}

- (void)didEnterBackgroundNotification:(NSNotification*)notification
{
    NSLog(@"Entered background notification called.");
    for (LeTemperatureAlarmService *service in self.connectedServices) {
        [service enteredBackground];
    }
}

- (void)didEnterForegroundNotification:(NSNotification*)notification
{
    NSLog(@"Entered foreground notification called.");
    for (LeTemperatureAlarmService *service in self.connectedServices) {
        [service enteredForeground];
    }
}


#pragma mark -
#pragma mark LeTemperatureAlarmProtocol Delegate Methods
/****************************************************************************/
/*				LeTemperatureAlarmProtocol Delegate Methods					*/
/****************************************************************************/
/** Broke the high or low temperature bound */
- (void) alarmService:(LeTemperatureAlarmService*)service didSoundAlarmOfType:(AlarmType)alarm
{
    if (![service isEqual:currentlyDisplayingService])
        return;
    
    NSString *title;
    NSString *message;
    
	switch (alarm) {
		case kAlarmLow:
			NSLog(@"Alarm low");
            title     = @"Alarm Notification";
            message   = @"Low Alarm Fired";
			break;
            
		case kAlarmHigh:
			NSLog(@"Alarm high");
            title     = @"Alarm Notification";
            message   = @"High Alarm Fired";
			break;
	}
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
    [alertView release];
}


/** Back into normal values */
- (void) alarmServiceDidStopAlarm:(LeTemperatureAlarmService*)service
{
    NSLog(@"Alarm stopped");
}


/** Current temp changed */
- (void) alarmServiceDidChangeTemperature:(LeTemperatureAlarmService*)service
{
    //if (service != currentlyDisplayingService)
    //    return;
    
    NSInteger currentTemperature = (int)[service temperature];
    if (currentTemperature & 4) {
        [remoteDelegate buttonPressed:BottomRight];
    }
    if (currentTemperature & 2) {
        [remoteDelegate buttonPressed:BottomLeft];
    }
    if (currentTemperature & 8) {
        [remoteDelegate buttonPressed:TopRight];
    }
    if (currentTemperature & 16) {
        [remoteDelegate buttonPressed:TopLeft];
    }
    if (currentTemperature & 1) {
        [remoteDelegate buttonPressed:Middle];
    }
    
}


/** Max or Min change request complete */
- (void) alarmServiceDidChangeTemperatureBounds:(LeTemperatureAlarmService*)service
{
    if (service != currentlyDisplayingService)
        return;
    
}


/** Peripheral connected or disconnected */
- (void) alarmServiceDidChangeStatus:(LeTemperatureAlarmService*)service
{
    if ( [[service peripheral] isConnected] ) {
        NSLog(@"Service (%@) connected", service.peripheral.name);
        if (![connectedServices containsObject:service]) {
            [connectedServices addObject:service];
        }
    }
    
    else {
        NSLog(@"Service (%@) disconnected", service.peripheral.name);
        if ([connectedServices containsObject:service]) {
            [connectedServices removeObject:service];
        }
    }
}


/** Central Manager reset */
- (void) alarmServiceDidReset
{
    [connectedServices removeAllObjects];
}

#pragma mark -
#pragma mark LeDiscoveryDelegate
/****************************************************************************/
/*                       LeDiscoveryDelegate Methods                        */
/****************************************************************************/
- (void) discoveryDidRefresh
{
    NSArray *devices;
    devices = [[LeDiscovery sharedInstance] foundPeripherals];
    for(CBPeripheral* peripheral in devices) {
        //peripheral = (CBPeripheral*)[devices objectAtIndex:([devices count]-1)];
        if (![peripheral isConnected]) {
            [[LeDiscovery sharedInstance] connectPeripheral:peripheral];
        } else {
            if ( currentlyDisplayingService != nil ) {
                [currentlyDisplayingService release];
                currentlyDisplayingService = nil;
            }
            
            currentlyDisplayingService = [self serviceForPeripheral:peripheral];
            [currentlyDisplayingService retain];
        }
    }
    
}

- (void) discoveryStatePoweredOff
{
    NSString *title     = @"Bluetooth Power";
    NSString *message   = @"You must turn on Bluetooth in Settings in order to use LE";
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
    [alertView release];
}

@end
