//
//  LeRemote.h
//  AddMusic
//
//  Created by Janek Mann on 11/11/2012.
//
//

#import "LeDiscovery.h"
#import "LeTemperatureAlarmService.h"

#import <Foundation/Foundation.h>

typedef enum {
    TopLeft  = 0,
    TopRight   = 1,
    BottomLeft = 2,
    BottomRight = 3,
    Middle = 4,
} ButtonType;

/****************************************************************************/
/*							UI protocols									*/
/****************************************************************************/
@protocol LeRemoteDelegate <NSObject>
- (void) buttonPressed:(ButtonType) button;
- (void) discoveryStatePoweredOff;
@end


@interface LeRemote : NSObject <LeDiscoveryDelegate, LeTemperatureAlarmProtocol> {
    
    
}
+ (id) sharedInstance;

@property (nonatomic, assign) id<LeRemoteDelegate>           remoteDelegate;
@property (retain, nonatomic) LeTemperatureAlarmService *currentlyDisplayingService;
@property (retain, nonatomic) NSMutableArray            *connectedServices;

/****************************************************************************/
/*								Actions										*/
/****************************************************************************/
- (void) setup;

@end
